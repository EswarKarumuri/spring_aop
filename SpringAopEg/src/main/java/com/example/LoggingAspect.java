package com.example;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LoggingAspect {
	
	@AfterReturning("execution(public String getUrl())")
	public void logBefore(JoinPoint joinPoint){
		System.out.println(joinPoint.getSignature().getName());
	}

}
